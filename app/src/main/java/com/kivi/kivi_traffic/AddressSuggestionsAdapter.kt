package com.kivi.kivi_traffic

import android.view.View
import com.kivi.kivi_traffic.databinding.ItemAddressSuggestionBinding
import com.kivi.widget.recycler.LazyAdapter

typealias AddressSuggestion = Pair<String, Boolean>

class AddressSuggestionsAdapter(onItemClickListener: OnItemClickListener<AddressSuggestion>) : LazyAdapter<AddressSuggestion, ItemAddressSuggestionBinding>(onItemClickListener) {

    override fun bindData(data: AddressSuggestion, binding: ItemAddressSuggestionBinding, position: Int, isLastItem: Boolean) {
        if (isLastItem) {
            binding.dividerView.visibility = View.GONE
            binding.root.nextFocusDownId = binding.root.id
        } else {
            binding.dividerView.visibility = View.VISIBLE
            binding.root.nextFocusDownId = -1
        }

        binding.llRoot.setOnClickListener { itemClickListener?.onLazyItemClick(data) }
        binding.ivLocation.visibility = if (!data.second) View.INVISIBLE else View.VISIBLE
        binding.tvCityName.text = data.first
    }

    override fun getLayoutId(): Int = R.layout.item_address_suggestion

}