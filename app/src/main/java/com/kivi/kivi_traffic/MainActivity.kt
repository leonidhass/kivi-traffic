package com.kivi.kivi_traffic

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.kivi.kivi_traffic.databinding.ActivityMainBinding
import com.kivi.kivi_traffic.utils.KeyboardManager
import com.kivi.widget.recycler.LazyAdapter
import com.kivi.widget.recycler.initWithLinLay

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var keyboardManager: KeyboardManager
    private lateinit var inputMethodManager: InputMethodManager

    private var isEditingOn = false

    private lateinit var addressSuggestionsFromAdapter: AddressSuggestionsAdapter
    private lateinit var addressSuggestionsToAdapter: AddressSuggestionsAdapter

    private lateinit var preferences: SharedPreferences
    private var addressSuggestions = mutableListOf<AddressSuggestion>()
    private var dbAddressesString = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboardManager = KeyboardManager(this)
        keyboardManager.connectActivity()

        preferences = getPreferences(Context.MODE_PRIVATE)
        addressSuggestions.add(AddressSuggestion("Мое местоположение", true))
        addressSuggestions.addAll(loadAddressesFromPrefs())

        initRecyclerViews()
        initClickListeners()
        initFocusListeners()
        initTextChangeListeners()
        initEditorActionListeners()
        initWebView()
        binding.wvContent.loadUrl("https://www.google.com/maps/dir/Десятинная 12/Сагайдачного 25б/data=!5m1!1e1")

//        webView.loadUrl(GOOGLE_MAPS_TRAFFIC_URL)

        //webView.stopLoading()

//        val etAddress1 = et_address_1
//        val etAddress2 = et_address_2
//
//        btn_search.setOnClickListener {
//            webView.stopLoading()
//            webView.loadUrl("https://www.google.com/maps/dir/${etAddress1.text}/${etAddress2.text}/data=!5m1!1e1")
//        }

    }

    private fun initRecyclerViews() {
        addressSuggestionsFromAdapter = AddressSuggestionsAdapter(object: LazyAdapter.OnItemClickListener<AddressSuggestion> {
            override fun onLazyItemClick(data: AddressSuggestion) {
                onRecyclerItemClick(data, binding.etFromWhere, binding.flFromWhere, binding.rvSuggestionsFrom)
            }
        })

        addressSuggestionsToAdapter = AddressSuggestionsAdapter(object: LazyAdapter.OnItemClickListener<AddressSuggestion> {
            override fun onLazyItemClick(data: AddressSuggestion) {
                onRecyclerItemClick(data, binding.etToWhere, binding.flToWhere, binding.rvSuggestionsTo)
            }
        })

        binding.rvSuggestionsFrom.initWithLinLay(LinearLayout.VERTICAL, addressSuggestionsFromAdapter, addressSuggestions)
        binding.rvSuggestionsTo.initWithLinLay(LinearLayout.VERTICAL, addressSuggestionsToAdapter, addressSuggestions)
    }

    private fun onRecyclerItemClick(data: AddressSuggestion, editText: EditText, frameLayout: FrameLayout, recyclerView: RecyclerView) {
        editText.apply {
            setText(data.first)
            setSelection(data.first.length)
        }

        disableInputAndHideRecycler(editText, frameLayout, recyclerView, true)
    }

    private fun initClickListeners() {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        binding.ivReverse.setOnClickListener { onIvReverseClick() }
        binding.ivSearch.setOnClickListener { onIvSearchClick() }
        binding.flFromWhere.setOnClickListener { onInputContainerClick(binding.etFromWhere, it, inputMethodManager) }
        binding.flToWhere.setOnClickListener { onInputContainerClick(binding.etToWhere, it, inputMethodManager) }
    }

    private fun initFocusListeners() {
        binding.etFromWhere.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                if (binding.ivReverse.isFocused || binding.ivSearch.isFocused) {
                    disableInputAndHideRecycler(binding.etFromWhere, binding.flFromWhere, binding.rvSuggestionsFrom)
                } else {
                    binding.flFromWhere.setBackgroundResource(R.drawable.shape_input_container_none)
                }
            }
        }

        binding.etToWhere.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                if (binding.ivReverse.isFocused || binding.ivSearch.isFocused) {
                    disableInputAndHideRecycler(binding.etToWhere, binding.flToWhere, binding.rvSuggestionsTo)
                } else {
                    binding.flToWhere.setBackgroundResource(R.drawable.shape_input_container_none)
                }
            }
        }

        binding.flFromWhere.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus && binding.etFromWhere.isEnabled) {
                binding.flFromWhere.setBackgroundResource(R.drawable.shape_input_container_clicked)
                binding.etFromWhere.requestFocus()
            }
        }

        binding.flToWhere.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus && binding.etToWhere.isEnabled) {
                binding.flToWhere.setBackgroundResource(R.drawable.shape_input_container_clicked)
                binding.etToWhere.requestFocus()
            }
        }
    }

    private fun initTextChangeListeners() {
        binding.etFromWhere.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(str: CharSequence, start: Int, before: Int, count: Int) {
                addressSuggestionsFromAdapter.swapData(addressSuggestions.filter { it.first.toLowerCase().startsWith(str.toString().toLowerCase()) })
            }
        })

        binding.etToWhere.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(str: CharSequence, start: Int, before: Int, count: Int) {
                addressSuggestionsToAdapter.swapData(addressSuggestions.filter { it.first.toLowerCase().startsWith(str.toString().toLowerCase()) })
            }
        })
    }

    private fun initEditorActionListeners() {
        binding.etFromWhere.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                if (event == null || !event.isShiftPressed) {
                    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                    disableInputAndHideRecycler(binding.etFromWhere, binding.flFromWhere, binding.rvSuggestionsFrom, true)
                    return@setOnEditorActionListener true
                }
            }

            return@setOnEditorActionListener false
        }

        binding.etToWhere.setOnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                if (event == null || !event.isShiftPressed) {
                    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                    disableInputAndHideRecycler(binding.etToWhere, binding.flToWhere, binding.rvSuggestionsTo, true)
                    return@setOnEditorActionListener true
                }
            }

            return@setOnEditorActionListener false
        }
    }

//    https://www.google.com/maps/dir/Мое местоположение/Янгеля 20/data=!5m1!1e1

    private fun onIvReverseClick() {
        val address1 = binding.etFromWhere.text.toString()
        val address2 = binding.etToWhere.text.toString()
        binding.etFromWhere.setText(address2)
        binding.etToWhere.setText(address1)
    }

    private fun onIvSearchClick() {
        val address1 = binding.etFromWhere.text.toString()
        val address2 = binding.etToWhere.text.toString()
        binding.wvContent.stopLoading()
        binding.wvContent.loadUrl("https://www.google.com/maps/dir/$address1/$address2/data=!5m1!1e1")

        var isSavedAnyAddress = false

        if (saveAddressToPrefs(address1.toLowerCase())) {
            addressSuggestions.add(1, AddressSuggestion(address1.toLowerCase(), false))
            isSavedAnyAddress = true
        }

        if (saveAddressToPrefs(address2.toLowerCase())) {
            addressSuggestions.add(1, AddressSuggestion(address2.toLowerCase(), false))
            isSavedAnyAddress = true
        }

        if (isSavedAnyAddress) {
            addressSuggestionsFromAdapter.swapData(addressSuggestions)
            addressSuggestionsToAdapter.swapData(addressSuggestions)
        }

    }

    private fun onInputContainerClick(editText: EditText, containerView: View, inputMethodManager: InputMethodManager) {
        isEditingOn = true
        containerView.setBackgroundResource(R.drawable.shape_input_container_clicked)
        editText.isEnabled = true
        editText.requestFocus()
        editText.setSelection(editText.text.length)
        inputMethodManager.toggleSoftInput(0, 0)

        when (editText.id) {
            R.id.et_from_where -> {
                addressSuggestionsFromAdapter.swapData(addressSuggestions.filter { it.first.toLowerCase().startsWith(editText.text.toString().toLowerCase()) })
                binding.rvSuggestionsFrom.visibility = View.VISIBLE
                editText.nextFocusDownId = binding.rvSuggestionsFrom.id
            }
            R.id.et_to_where -> {
                addressSuggestionsToAdapter.swapData(addressSuggestions.filter { it.first.toLowerCase().startsWith(editText.text.toString().toLowerCase()) })
                binding.rvSuggestionsTo.visibility = View.VISIBLE
                editText.nextFocusDownId = binding.rvSuggestionsTo.id
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        binding.wvContent.webChromeClient = getWebChromeClient()
        binding.wvContent.setInitialScale(1)

        binding.wvContent.settings.javaScriptEnabled = true
        binding.wvContent.settings.loadWithOverviewMode = true
        binding.wvContent.settings.useWideViewPort = true
    }

    private fun getWebChromeClient(): WebChromeClient {
        return object: WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                if (newProgress < 100) {
                    binding.progressBar.visibility = View.VISIBLE
                } else {
                    binding.progressBar.visibility = View.GONE
                    view?.visibility = View.VISIBLE
                }
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    private fun saveAddressToPrefs(address: String): Boolean {
        var isSaved = true
        val isAddressSuggestionsContains = addressSuggestions.map { it.first.toLowerCase() }.contains(address)

        preferences.edit().apply {
            if (dbAddressesString.isEmpty() && !isAddressSuggestionsContains) {
                dbAddressesString = address
            } else {
                val cities = dbAddressesString.split(",")
                if (cities.contains(address) || isAddressSuggestionsContains) {
                    isSaved = false
                    return@apply
                }

                dbAddressesString = "$address,$dbAddressesString"
            }

            putString(PREFS_ADDRESSES, dbAddressesString)
            apply()
        }

        return isSaved
    }

    private fun loadAddressesFromPrefs(): List<AddressSuggestion> {
        val citiesString = preferences.getString(PREFS_ADDRESSES, "")
        return if (citiesString.isNullOrEmpty()) {
            listOf()
        } else {
            dbAddressesString = citiesString
            citiesString.split(",").map { AddressSuggestion(it, false) }
        }
    }

    override fun onBackPressed() {
        if (!keyboardManager.isOpened && isEditingOn) {
            if (binding.rvSuggestionsFrom.visibility == View.VISIBLE) {
                disableInputAndHideRecycler(binding.etFromWhere, binding.flFromWhere, binding.rvSuggestionsFrom, true)
            }

            if (binding.rvSuggestionsTo.visibility == View.VISIBLE) {
                disableInputAndHideRecycler(binding.etToWhere, binding.flToWhere, binding.rvSuggestionsTo, true)
            }

            return
        }

        super.onBackPressed()
    }

    private fun disableInputAndHideRecycler(editText: EditText, frameLayout: FrameLayout, recyclerView: RecyclerView, isNeedFocusContainer: Boolean = false) {
        isEditingOn = false

        editText.apply {
            isEnabled = false
            nextFocusDownId = id
        }

        recyclerView.visibility = View.GONE

        frameLayout.setBackgroundResource(R.drawable.selector_grey_blue_rectangle_m4dp)
        if (isNeedFocusContainer) {
            frameLayout.requestFocus()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        keyboardManager.disconnectActivity()
    }

    companion object {
        const val GOOGLE_MAPS_TRAFFIC_URL = "https://www.google.com/maps/place/data=!5m1!1e1/?force=canvas"
        const val PREFS_ADDRESSES = "pref_addresses"
    }

    // OK

    /*
    https://www.google.com/maps/dir/улица+Академика+Янгеля,+20,+Киев,+город+Киев/ул.+Петра+Сагайдачного,+25Б,+Киев,+02000/@50.4536465,30.4667128,14z/data=!5m1!1e1
    https://www.google.com/maps/dir/Академика Янгеля 20/Петра Сагайдачного 25Б/@50.4536465,30.4667128,14z/data=!5m1!1e1
    https://www.google.com/maps/dir/Академика Янгеля 20/Петра Сагайдачного 25Б/data=!5m1!1e1
    https://www.google.com/maps/dir/Киев Академика Янгеля 20/Киев Петра Сагайдачного 25Б/data=!5m1!1e1
    */

}