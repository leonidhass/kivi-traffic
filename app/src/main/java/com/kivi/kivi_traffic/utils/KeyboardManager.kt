package com.kivi.kivi_traffic.utils

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewTreeObserver

class KeyboardManager(activity: Activity) {

    var isOpened = false
        private set

    private val rootView: View = activity.findViewById<View>(android.R.id.content)

    private val globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        val rect = Rect().apply { rootView.getWindowVisibleDisplayFrame(this) }
        val screenHeight = rootView.height
        val keypadHeight = screenHeight - rect.bottom
        isOpened = keypadHeight > screenHeight * 0.15
    }

    fun connectActivity() {
        rootView.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    }

    fun disconnectActivity() {
        rootView.viewTreeObserver.removeOnGlobalLayoutListener(globalLayoutListener)
    }

}